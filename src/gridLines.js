c3.gridLines = function() {
    return c3.component('gridLines')
        .extend(c3.axis())
        .axisConstructor(function() {
            var tickSize;
            switch (this.orient()) {
                case 'left': tickSize = -this.width(); break;
                case 'right': tickSize = this.width(); break;
                case 'top': tickSize = -this.height(); break;
                case 'bottom': tickSize = this.height(); break;
            }
            return d3.svg.axis().tickSize(tickSize).tickFormat('');
        });
};
