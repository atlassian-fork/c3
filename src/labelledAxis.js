c3.labelledAxis = function() {
    return c3.component('labelledAxis')
        .extend(c3.axis())
        .extend({
            text: c3.prop('')
        })
        .extend(function() {
            // Render label
            var text = this.text();
            var label = c3.singular()
                .elementTag('text')
                .elementClass('axis-label')
                .enter(function(event) {
                    event.selection.attr('text-anchor', 'middle');
                })
                .update(function(event) {
                    event.selection.text(text);
                });
            var labelElement = label(this.selection()).elements();

            // Positioning
            var x = 0,
                y = 0,
                degrees = 0;
            var bBox = labelElement.node().getBBox();
            switch(this.orient()) {
                case 'left':
                    degrees = 270;
                    x = -1 * this.width() + bBox.height; // bbox not yet rotated
                    y = this.height() / 2;
                    break;
                case 'right':
                    degrees = 90;
                    x = this.width() - bBox.height;
                    y = this.height() / 2;
                    break;
                case 'top':
                    degrees = 0;
                    x = this.width() / 2;
                    y = -1 * this.height() + bBox.height;
                    break;
                default: // bottom
                    degrees = 0;
                    x = this.width() / 2;
                    y = this.height() - bBox.height / 2;
            }
            labelElement.attr('transform', 'translate(' + x + ', ' + y +') rotate(' + degrees + ', 0, 0)');
        });
};
