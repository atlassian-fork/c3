c3.withData = function() {
    return c3.component('withData')
        .extend({
            data: c3.inherit('data', []),
            dataKey: c3.inherit('dataKey')
        });
};
