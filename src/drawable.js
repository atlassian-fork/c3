/**
 * A component that maps data to elements, using d3's enter and exit joins
 * to add/remove elements as necessary.
 */
c3.drawable = function() {
    return c3.component('drawable')
        .extend(c3.withData())
        .extend(c3.withElements())
        .extend(function() {
            var binding = this.dataBinding();
            this.enter({ selection: this.drawEnter(binding.enter()) });
            this.exit({ selection: this.drawExit(binding.exit()) });
            this.update({ selection: binding });
        })
        .extend({
            enter:         c3.event(),
            exit:          c3.event(),
            update:        c3.event(),
            dataFilter:    c3.prop(_.identity),
            dataBinding: function() {
                var data = this.dataFilter()(this.data());
                return this.elements().data(data, this.dataKey());
            },
            drawEnter: function(enter) {
                var elements = enter.append(this.elementTag());
                var elementClass = this.elementClass();
                if (elementClass) {
                    elements.classed(elementClass, true);
                }
                return elements;
            },
            drawExit: function(exit) {
                return exit.remove();
            }
        });
};
