/*
 * Clustering algorithm
 * Requires figue.js
 */

var clusterVectors = function (rawVectors, vectorLabels, clusterThreshold, xScale, yScale) {
    // resolve scaled vectors
    var scaledVectors = _.map(rawVectors, function(vector) {
        return [
            xScale(vector[0]),
            yScale(vector[1])
        ];
    });

    // trim branches if they exceed the threshold distance, mark pruned branches as a 'cluster'
    function pruneAtDistanceThreshold(tree,  result) {
        if (!tree) return;
        if (tree.dist && tree.dist > clusterThreshold) {
            pruneAtDistanceThreshold(tree.left, result);
            pruneAtDistanceThreshold(tree.right, result);
        } else {
            if (tree.left || tree.right) {
                result.push({
                    point:      [xScale.invert(tree.centroid[0]), yScale.invert(tree.centroid[1])],
                    size:       tree.size,
                    distance:   tree.dist,
                    cluster:    true
                });
            } else {
                result.push({
                    point:      [xScale.invert(tree.centroid[0]), yScale.invert(tree.centroid[1])],
                    size:       tree.size,
                    distance:   tree.dist,
                    cluster:    false
                });
            }
        }
    }

    // calculate hierarchical agglomerate of the vector
    var agglomerate = figue.agglomerate(vectorLabels, scaledVectors, figue.MAX_DISTANCE, figue.SINGLE_LINKAGE);

    // prune the tree
    var result = [];
    pruneAtDistanceThreshold(agglomerate, result);

    return result;
};
