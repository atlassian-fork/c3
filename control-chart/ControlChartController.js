/* global d3, c3, controlChartData, ControlChartDataModel */
"use strict";

function init() {
    var dataModel = new ControlChartDataModel(controlChartData);

    var controlChart = function () {
        var xAxis = c3.axis()
            .xScaleConstructor(d3.time.scale)
            .height(30);
        var yAxis = c3.axis()
            .orient('left')
            .width(30);
        var circlePlot = function () {
            return c3.circlePlot()
                .xAccessor(function (d) {
                    return d.point[0];
                })
                .yAccessor(function(d) {
                    return d.point[1];
                });
        };

        var plots = c3.layerable()
            .addLayer('grid', c3.gridLines()
                .orient('left')
            )
            .addLayer('aggregate-mean-outline', c3.linePlot()
                .data(dataModel.aggregateMeanSeries)
                .elementClass('control-chart-aggregate-mean-outline')
            )
            .addLayer('aggregate-mean', c3.linePlot()
                .data(dataModel.aggregateMeanSeries)
                .elementClass('control-chart-aggregate-mean')
            )
            .addLayer('mean', c3.linePlot()
                .data(dataModel.standardMeanSeries)
                .elementClass('control-chart-mean')
            )
            .addLayer('issues', circlePlot()
                .extend({
                    data: function() {
                        return dataModel.getClusteredIssueSeries(10, this.xScale(), this.yScale()).unclustered;
                    }
                })
                .elementClass('issue')
            )
            .addLayer('issue-clusters', circlePlot()
                .extend({
                    data: function() {
                        return dataModel.getClusteredIssueSeries(10, this.xScale(), this.yScale()).clustered;
                    }
                })
                .elementClass('cluster')
                .radiusAccessor(function (d) {
                    return d.distance * 2;
                })
            );

        return c3.borderLayout()
            .data(dataModel.values)
            .xScaleConstructor(d3.time.scale)
            .height(400)
            .center(plots)
            .south(xAxis)
            .west(yAxis);
    }

    var chart = controlChart();

    // Apply selection
    chart(d3.select('#control-chart'));
}

init();